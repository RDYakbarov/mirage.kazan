/**
 * Created by ilartech on 05.09.17.
 */
jQuery(document).ready(function($) {
    $('.fadeOut').owlCarousel({
        items: 1,
        margin: 0,
        dots: false,
        mouseDrag: false,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeIn',
        loop: true,
        slideBy: 1,
        slideSpeed: 100,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause: true,
        navContainer: '#customNav',
        responsive: {
            0: {
                items:1,
            },
            600: {
                items:1,
            },
            1000: {
                items:1
            }
        }
    });
    $( ".owl-prev").html('<div class="nav-prev"><i class="arrow-prev"></i></div>');
    $( ".owl-next").html('<div class="nav-next"><i class="arrow-next"></i></div>');
});
jQuery(document).ready(function($) {
    $('.nav_studio').owlCarousel({
        items: 1,
        margin: 0,
        dots: false,
        mouseDrag: false,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeIn',
        loop: true,
        slideBy: 1,
        slideSpeed: 100,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause: true,
        navContainer: '#nav_studio',
        responsive: {
            0: {
                items:1,
            },
            600: {
                items:1,
            },
            1000: {
                items:1
            }
        }
    });
    $( ".owl-prev").html('<div class="nav-prev"><i class="arrow-prev"></i></div>');
    $( ".owl-next").html('<div class="nav-next"><i class="arrow-next"></i></div>');
});
jQuery(document).ready(function($) {
    $('.nav_business').owlCarousel({
        items: 1,
        margin: 0,
        dots: false,
        mouseDrag: false,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeIn',
        loop: true,
        slideBy: 1,
        slideSpeed: 100,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause: true,
        navContainer: '#nav_business',
        responsive: {
            0: {
                items:1,
            },
            600: {
                items:1,
            },
            1000: {
                items:1
            }
        }
    });
    $( ".owl-prev").html('<div class="nav-prev"><i class="arrow-prev"></i></div>');
    $( ".owl-next").html('<div class="nav-next"><i class="arrow-next"></i></div>');
});
jQuery(document).ready(function($) {
    $('.nav_representative').owlCarousel({
        items: 1,
        margin: 0,
        dots: false,
        mouseDrag: false,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeIn',
        loop: true,
        slideBy: 1,
        slideSpeed: 100,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause: true,
        navContainer: '#nav_representative',
        responsive: {
            0: {
                items:1,
            },
            600: {
                items:1,
            },
            1000: {
                items:1
            }
        }
    });
    $( ".owl-prev").html('<div class="nav-prev"><i class="arrow-prev"></i></div>');
    $( ".owl-next").html('<div class="nav-next"><i class="arrow-next"></i></div>');
});
jQuery(document).ready(function($) {
    $('.nav_apartment').owlCarousel({
        items: 1,
        margin: 0,
        dots: false,
        mouseDrag: false,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeIn',
        loop: true,
        slideBy: 1,
        slideSpeed: 100,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause: true,
        navContainer: '#nav_apartment',
        responsive: {
            0: {
                items:1,
            },
            600: {
                items:1,
            },
            1000: {
                items:1
            }
        }
    });
    $( ".owl-prev").html('<div class="nav-prev"><i class="arrow-prev"></i></div>');
    $( ".owl-next").html('<div class="nav-next"><i class="arrow-next"></i></div>');
});
jQuery(document).ready(function($) {
    $('.nav_presidential').owlCarousel({
        items: 1,
        margin: 0,
        dots: false,
        mouseDrag: false,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeIn',
        loop: true,
        slideBy: 1,
        slideSpeed: 100,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause: true,
        navContainer: '#nav_presidential',
        responsive: {
            0: {
                items:1,
            },
            600: {
                items:1,
            },
            1000: {
                items:1
            }
        }
    });
    $( ".owl-prev").html('<div class="nav-prev"><i class="arrow-prev"></i></div>');
    $( ".owl-next").html('<div class="nav-next"><i class="arrow-next"></i></div>');
});
jQuery(document).ready(function($) {
    $('.s_offers').owlCarousel({
        items: 3,
        margin: 30,
        dots: false,
        mouseDrag: false,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeIn',
        loop: true,
        slideBy: 1,
        smartSpeed: 800,
        autoplay:false,
        autoplayTimeout:5000,
        autoplayHoverPause: true,
        autoWidth: true,
        nav: true,
        navContainer: '#s_offers',
        responsive: {
            0: {
                items:1,
            },
            600: {
                items:1,
            },
            1000: {
                items:3
            }
        }
    });
    $( ".owl-prev").html('<div class="nav-prev"><i class="arrow-prev"></i></div>');
    $( ".owl-next").html('<div class="nav-next"><i class="arrow-next"></i></div>');
});
$(document).ready(function () {
    var owl = $('.b-item-slider');
    owl.owlCarousel({
        margin: 10,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeIn',
        nav: true,
        navContainer: '#b-item-slider',
        loop: true,
        slideBy: 4,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    $( ".owl-prev").html('<div class="nav-prev"><div class="ellipses-prev"><span class="b-item-ellipse"></span><span class="b-item-ellipse"></span><span class="b-item-ellipse"></span></div><i class="arrow-prev"></i></div>');
    $( ".owl-next").html('<div class="nav-next"><div class="ellipses-next"><span class="b-item-ellipse"></span><span class="b-item-ellipse"></span><span class="b-item-ellipse"></span></div><i class="arrow-next"></i></div>');
});
jQuery(document).ready(function($) {
    $('.c-content-edge1').owlCarousel({
        items: 1,
        margin: 0,
        dots: false,
        mouseDrag: false,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeIn',
        loop: true,
        slideBy: 1,
        slideSpeed: 100,
        // autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause: true,
        navContainer: '#room-slider-nav1',
        responsive: {
            0: {
                items:1,
            },
            600: {
                items:1,
            },
            1000: {
                items:1
            }
        }
    });
    $( ".owl-prev").html('<div class="nav-prev"><i class="arrow-prev"></i></div>');
    $( ".owl-next").html('<div class="nav-next"><i class="arrow-next"></i></div>');
});
jQuery(document).ready(function($) {
    $('.c-content-edge2').owlCarousel({
        items: 1,
        margin: 0,
        dots: false,
        mouseDrag: false,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeIn',
        loop: true,
        slideBy: 1,
        slideSpeed: 100,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause: true,
        navContainer: '#room-slider-nav2',
        responsive: {
            0: {
                items:1,
            },
            600: {
                items:1,
            },
            1000: {
                items:1
            }
        }
    });
    $( ".owl-prev").html('<div class="nav-prev"><i class="arrow-prev"></i></div>');
    $( ".owl-next").html('<div class="nav-next"><i class="arrow-next"></i></div>');
});
jQuery(document).ready(function($) {
    $('.c-content-edge3').owlCarousel({
        items: 1,
        margin: 0,
        dots: false,
        mouseDrag: false,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeIn',
        loop: true,
        slideBy: 1,
        slideSpeed: 100,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause: true,
        navContainer: '#room-slider-nav3',
        responsive: {
            0: {
                items:1,
            },
            600: {
                items:1,
            },
            1000: {
                items:1
            }
        }
    });
    $( ".owl-prev").html('<div class="nav-prev"><i class="arrow-prev"></i></div>');
    $( ".owl-next").html('<div class="nav-next"><i class="arrow-next"></i></div>');
});
jQuery(document).ready(function($) {
    $('.c-content-edge4').owlCarousel({
        items: 1,
        margin: 0,
        dots: false,
        mouseDrag: false,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeIn',
        loop: true,
        slideBy: 1,
        slideSpeed: 100,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause: true,
        navContainer: '#room-slider-nav4',
        responsive: {
            0: {
                items:1,
            },
            600: {
                items:1,
            },
            1000: {
                items:1
            }
        }
    });
    $( ".owl-prev").html('<div class="nav-prev"><i class="arrow-prev"></i></div>');
    $( ".owl-next").html('<div class="nav-next"><i class="arrow-next"></i></div>');
});
jQuery(document).ready(function($) {
    $('.c-content-edge5').owlCarousel({
        items: 1,
        margin: 0,
        dots: false,
        mouseDrag: false,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeIn',
        loop: true,
        slideBy: 1,
        slideSpeed: 100,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause: true,
        navContainer: '#room-slider-nav5',
        responsive: {
            0: {
                items:1,
            },
            600: {
                items:1,
            },
            1000: {
                items:1
            }
        }
    });
    $( ".owl-prev").html('<div class="nav-prev"><i class="arrow-prev"></i></div>');
    $( ".owl-next").html('<div class="nav-next"><i class="arrow-next"></i></div>');
});
jQuery(document).ready(function($) {
    $('.c-content-edge6').owlCarousel({
        items: 1,
        margin: 0,
        dots: false,
        mouseDrag: false,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeIn',
        loop: true,
        slideBy: 1,
        slideSpeed: 100,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause: true,
        navContainer: '#room-slider-nav6',
        responsive: {
            0: {
                items:1,
            },
            600: {
                items:1,
            },
            1000: {
                items:1
            }
        }
    });
    $( ".owl-prev").html('<div class="nav-prev"><i class="arrow-prev"></i></div>');
    $( ".owl-next").html('<div class="nav-next"><i class="arrow-next"></i></div>');
});