/************ rooms tab start **************/
function openLink(evt, animName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("room");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
        tablinks[i].className = tablinks[i].className.replace("", "");
    }
    document.getElementById(animName).style.display = "block";
    evt.currentTarget.className += " w3-red" , "active";
}
/************ rooms tab end **************/

/************ hover rooms start **************/
$('.wrap-btn').click(function (e) {
    e.preventDefault();
    $('.wrap-btn').removeClass('active');
    $(this).addClass('active');
});
/************ hover rooms end **************/

/************ ymap start **************/
ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [55.7940, 49.1038],
            zoom: 18
        }, {
            searchControlProvider: 'yandex#search'
        }),

        /*// Создаём макет содержимого.
        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
        ),*/
        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: '',
            balloonContent: 'Это красивая метка'
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: '',
            // Размеры метки.
            iconImageSize: [30, 42],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-5, -38]
        });

    myMap.behaviors.disable('scrollZoom');
    // map.behaviors.disable('multiTouch');
    myMap.geoObjects.add(myPlacemark)
});

wow = new WOW(
    {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
            console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
    }
);
wow.init();
document.getElementById('moar').onclick = function() {
    var section = document.createElement('section');
    section.className = 'section--purple wow fadeInDown';
    this.parentNode.insertBefore(section, this);
};

/*
See https://codepen.io/MarcelSchulz/full/lCvwq

The effect doens't appear as nice when viewing in split view :-)

Fully working version can also be found at (http://schulzmarcel.de/x/drafts/parallax).

*/
var selectid;

$(document).on('click','.select',function(){
    selectid = $(this).index('.select');
    var adjust = ($(this).hasClass('collapsed')) ? 1 : 0;

    console.log((($(this).children('.option').length) * $(this).children('.option').height()) * (adjust) + 40 + 'px');

    $(this).animate({'height':(($(this).children('.option').length) * $(this).children('.option').height()) * (adjust) + 40 + 'px'},100);
    $(this).toggleClass('collapsed');
});

$(document).on('click','.option',function(){
    var val = $(this).text();
    $(this).parent().find('.shown').text(val);
    selectid = -1;
});

var desiredElement = document.querySelectorAll('.main-map');

function switchTheme() {
    for (var i = 0; i < desiredElement.length; i++) {
        if (desiredElement[i].classList.contains('main-map')) {
            desiredElement[i].classList.remove('main-map');
            desiredElement[i].classList.add('toggle-map');
        } else if (desiredElement[i].classList.contains('toggle-map')) {
            desiredElement[i].classList.remove('toggle-map');
            desiredElement[i].classList.add('main-map');
        }
    }
}

